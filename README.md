| Version | Status |
| ------ | ------ |
| **2019.8.15.0** | [![pipeline status](http://svr-gitlab.grupocr.local/PI2K/docker/docker-git/badges/2019.8.15.0/pipeline.svg)](http://svr-gitlab.grupocr.local/PI2K/docker/docker-git/commits/2019.8.15.0) |
| **2019.8.9.0** | [![pipeline status](http://svr-gitlab.grupocr.local/PI2K/docker/docker-git/badges/2019.8.9.0/pipeline.svg)](http://svr-gitlab.grupocr.local/PI2K/docker/docker-git/commits/2019.8.9.0) |
| **19** | [![pipeline status](http://svr-gitlab.grupocr.local/PI2K/docker/docker-git/badges/19/pipeline.svg)](http://svr-gitlab.grupocr.local/PI2K/docker/docker-git/commits/19) |

---

# docker-git

Docker image with: Git
